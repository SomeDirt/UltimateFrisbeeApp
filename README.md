# ULTIMATE FRISBEE APP.

This is an in-development ultimate frisbee scheduler app that is being made for FrisIB.
Useful dependencies that are currently in the project

    - Ionic 2
    - Angular 2
    - NodeJS
    - TypeScript
    - Firebase


## How to set up.

First, clone this repo.

Then, move into the folder, and open a command prompt window.
    - Hold shift and right click, you should see an option for that

Choose "open command prompt here"

Type in "npm install"

Grab a cup of coffee while it installs all dependencies

## How do I test/edit/run?

test: learn ionic

edit: learn angular/typescript/node

run: learn more ionic

## How do I contribute?

learn git.


