import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RelativeTime } from './relative-time/relative-time';

@NgModule({
  declarations: [
    RelativeTime,
  ],
  imports: [
    IonicPageModule .forChild(RelativeTime),
  ],
  exports: [
    RelativeTime,
  ]
})
export class PipesModule {}
