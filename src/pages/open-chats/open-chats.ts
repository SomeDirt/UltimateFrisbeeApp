import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { OpenChat, ChatlistService } from "../../providers/chatlist-service/chatlist-service";

import { Chat } from "../chat/chat";
import { ChatCreatePage } from "../chat-create/chat-create";
import firebase from 'firebase';


@IonicPage()
@Component({
    selector: 'page-open-chats',
    templateUrl: 'open-chats.html',
})
export class OpenChatsPage {

    //this is the array of open chats that the user has
    public chatList: Array<OpenChat>;
    //this is a loaded version of it so that when searching,
    //it doesn't tax the database as much 
    public loadedChatList: Array<OpenChat>;

    constructor(public navCtrl: NavController,
        public chatlistService: ChatlistService,
    ) {

        this.chatList = chatlistService.getChatList(firebase.auth().currentUser.uid);
        this.loadedChatList = this.chatList;

    }

    ionViewDidLoad() {
        // console.log('ionViewDidLoad OpenChatsPage');
    }

    //this re-initializes the chat list.
    initializeChatList() {
        this.chatList = this.loadedChatList;
    }

    //this searches. It removes items from chatlist that don't match
    //the search term.
    getOpenChats(searchbar) {
        //reset the items
        this.initializeChatList();

        //temp equal to search bar
        var tmp = searchbar.target.value;

        //if empty dont filter
        if (!tmp || !this.chatList) {
            return;
        }

        this.chatList = this.chatList.filter((f) => {
            if (f.chatName.toLowerCase().indexOf(tmp.toLowerCase()) > -1) {
                return true;
            } else {
                if (this.isInChat(f, tmp)) {
                    return true;
                }
            }

            return false;

        });

    }

    /**
     * checks to see if the user is in the chat.
     * 
     * @param chat This is the open chat
     * @param tmp This is the string search term.
     */
    isInChat(chat: OpenChat, tmp) {
        chat.userList.forEach(element => {
            if (element.toLowerCase().indexOf(tmp.toLowerCase()) > -1) {
                return true;
            }

            return false;
        });
    }


    //if a user taps on a chat
    chatTapped(event, chatName, chatId) {
        this.navCtrl.push(Chat, {
            chatId: chatId,
            chatName: chatName
        });

    }

    //this moves to the create a chat page
    createNewChat() {
        this.navCtrl.push(ChatCreatePage);
    }

}
