import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenChatsPage } from './open-chats';
import { ChatlistService } from '../../providers/chatlist-service/chatlist-service';

@NgModule({
    declarations: [
        OpenChatsPage,
    ],
    imports: [
        IonicPageModule.forChild(OpenChatsPage),
    ],
    exports: [
        OpenChatsPage
    ],
    providers: [
        ChatlistService
    ]
})
export class OpenChatsPageModule { }
