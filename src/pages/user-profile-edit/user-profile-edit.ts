import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { EmailValidator } from '../../validators/email';

import { ContactService } from "../../providers/contact-service/contact-service";
import { UserDocumentService } from "../../providers/user-document-service/user-document-service";

/**
 * Generated class for the UserProfileEditPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-user-profile-edit',
    templateUrl: 'user-profile-edit.html',
})
export class UserProfileEditPage {
    userId;
    userProfile;
    editForm: FormGroup;
    mismatchedPasswords: false;
    image;


    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public userProfiles: ContactService,
        public formBuilder: FormBuilder,
        public userPic: UserDocumentService,
    ) {

        this.userId = navParams.get('userId');
        this.userProfile = userProfiles.getUserProfile(this.userId);


        this.editForm = formBuilder.group({

            //names used in chat.
            name: ['', Validators.compose([Validators.minLength(1)])],

            //phone number
            phoneNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],


            //validators and stuff,
            email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
            password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
            confirmPassword: ['', Validators.required]
        }, { validator: this.matchingPasswords('password', 'confirmPassword') });

    }

    ionViewDidLoad() {
    }


    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];

            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }


    //when the submit button is pressed.
    changeProfile() {
        //if a new image was uploaded,
        if(this.image != null){
            //upload the image to firebase storage, also changes the profile thing.
            this.userPic.uploadNewProfileImage(this.image);
            
        }



        console.log("hello");
    }

    takePhoto(){
        this.userPic.takePhoto().then((imageData) =>{
            this.image = imageData;
        });
    }

    selectPhoto(){
        this.userPic.selectPhoto().then((imageData) =>{
            this.image = imageData;
        });
    }







}
