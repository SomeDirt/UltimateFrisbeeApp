import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfileEditPage } from './user-profile-edit';
import { ContactService } from "../../providers/contact-service/contact-service";
import { Camera } from '@ionic-native/camera';

@NgModule({
  declarations: [
    UserProfileEditPage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfileEditPage),
  ],
  exports: [
    UserProfileEditPage,
  ],
  providers: [
    ContactService,
    Camera,
    
  ]
})
export class UserProfileEditModule {}
