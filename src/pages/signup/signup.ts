import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { EmailValidator } from '../../validators/email';


import { HomePage } from "../home/home";
import * as firebase from 'firebase';
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})


export class SignupPage {
  public signupForm:FormGroup;
  public loading:Loading;
  mismatchedPasswords : false;
  


  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, 
    public alertCtrl: AlertController, public formBuilder: FormBuilder, 
    public authProvider: AuthProvider) {

    this.signupForm = formBuilder.group({
      
      //names used in chat.
      name: ['', Validators.compose([Validators.minLength(2)])],

      //phone number
      phoneNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],

      //validators and stuff,
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      confirmPassword: ['', Validators.required]
    }, {validator: this.matchingPasswords('password', 'confirmPassword')});



  
  } //end of constructor


  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  signupUser(){

    var name = this.signupForm.value.name;

    if (!this.signupForm.valid){
      console.log(this.signupForm.value);
    } else {
      this.authProvider.signupUser(this.signupForm.value.email, this.signupForm.value.password, this.signupForm.value.phoneNumber, name)
      .then(() => {
        this.loading.dismiss().then( () => {


          
          firebase.auth().currentUser.updateProfile({
            displayName:  name,
            photoURL: './assets/user.jpg'
          }).then(function() {
            // Update successful.
          }, function(error) {
            // An error happened.
          });


          this.navCtrl.setRoot(HomePage);
        });
      }, (error) => {
        this.loading.dismiss().then( () => {
          var errorMessage: string = error.message;
          let alert = this.alertCtrl.create({
            message: errorMessage,
            buttons: [{ text: "Ok", role: 'cancel' }]
          });
          alert.present();
        });
      });


      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

}
