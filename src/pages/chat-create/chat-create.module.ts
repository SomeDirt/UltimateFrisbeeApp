import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatCreatePage } from './chat-create';
import { ChatlistService } from '../../providers/chatlist-service/chatlist-service';

@NgModule({
    declarations: [
        ChatCreatePage,
    ],
    imports: [
        IonicPageModule.forChild(ChatCreatePage),
    ],
    exports: [
        ChatCreatePage
    ],
    providers: [
        ChatlistService
    ]
})
export class ChatCreatePageModule { }
