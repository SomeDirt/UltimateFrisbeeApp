import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController} from 'ionic-angular';
import { User, ContactService } from "./../../providers/contact-service/contact-service";
import { ChatlistService } from "./../../providers/chatlist-service/chatlist-service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Chat } from "./../chat/chat";
import * as firebase from "firebase";


/**
 * Generated class for the ChatCreatePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()

@Component({
    selector: 'page-chat-create',
    templateUrl: 'chat-create.html',
})
export class ChatCreatePage {

    chatName: string;
    userList: Array<User>;
    loadedUserList: Array<User>;
    chosenUsers: Array<User>;

    chosen = true;

    public newChatForm: FormGroup;

    constructor(public navCtrl: NavController,
        public viewCtrl: ViewController,
        public contacts: ContactService,
        public formBuilder: FormBuilder,
        public chatlistService: ChatlistService,

    ) {

        //update the user list.
        this.userList = contacts.getUserList();
        this.loadedUserList = this.userList;
        this.chosenUsers = new Array<User>();


        this.newChatForm = formBuilder.group({
            chatName: ['', Validators.compose([Validators.minLength(1), Validators.required])]
        });


    }


    //checks to see if the chosen user list is empty or not
    chosenNotEmpty() {
        return this.chosenUsers.length != 0;
    }

    //checks to see if the user is inside the user
    isInChosen(user) {
        return this.chosenUsers.indexOf(user) != -1;
    }

    isChosen() {
        return true;
    }

    //this will be the toggle thing. On change, it will remove the user if it is already in, or add if not.
    userChange(event, user) {
        if (this.isInChosen(user)) {
            var index = this.chosenUsers.indexOf(user);
            this.chosenUsers.splice(index, 1);

        } else {
            this.chosenUsers.push(user);
        }
    }


    //this will reinitialize the list when searching for things
    initializeUsers() {
        this.userList = this.loadedUserList;
    }

    getUsers(searchbar) {
        //reset all items
        this.initializeUsers();


        //tmp is equal to the searchbar
        var tmp = searchbar.target.value;


        //if empty don't filter the items
        if (!tmp) {
            return;
        }

        this.userList = this.userList.filter((f) => {
            if (f.userName && tmp) {

                //search by name
                if (f.userName.toLowerCase().indexOf(tmp.toLowerCase()) > -1) {
                    return true;
                }
                //search by email
                else if (f.email.toLowerCase().indexOf(tmp.toLowerCase()) > -1) {
                    return true;

                }
                return false;
            }

        });

    }




    createChat() {
        //if it is valid, create a new chat and stuff
        if (!(this.newChatForm.valid && this.chosenNotEmpty())) {
            console.log("invalid");
        } else {
            console.log("valid");

            //add yourself into users.

            //takes out useful information
            var userList = new Array<String>();
            var userIDList = new Array<String>();

            this.chosenUsers.forEach(element => {
                userList.push(element.userName);
                userIDList.push(element.userId);
            });

            userList.push(firebase.auth().currentUser.displayName);
            userIDList.push(firebase.auth().currentUser.uid);

            //gets the chat name
            let name = this.newChatForm.value.chatName;


            //chat name, userList, userIdList
            var newChatID = this.chatlistService.createNewChat(name, userList, userIDList);






            //remove chatcreate from nav stack, so it goes back to open chats page
            this.navCtrl.push(Chat, {
                chatId: newChatID,
                chatName: name,
            }).then(() => {
                // first we find the index of the current view controller:
                const index = this.viewCtrl.index;
                // then we remove it from the navigation stack
                this.navCtrl.remove(index);
            });



        }
    }











}
