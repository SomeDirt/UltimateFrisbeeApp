import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfilePage } from './userProfile';
import { ContactService } from "../../providers/contact-service/contact-service";
import { ImageUploadModule } from "angular2-image-upload";

import { AgmCoreModule } from 'angular2-google-maps/core';


@NgModule({
  declarations: [
    UserProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfilePage),
    ImageUploadModule.forRoot(),
    AgmCoreModule.forRoot()
  ],
  exports: [
    UserProfilePage
  ],
  providers: [
    ContactService
  ]

})
export class UserProfileModule {}
