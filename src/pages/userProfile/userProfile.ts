import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading,
  LoadingController,
  AlertController } from 'ionic-angular';
import { ContactService } from "./../../providers/contact-service/contact-service";
import { UserProfileEditPage } from "..//user-profile-edit/user-profile-edit";


// import { Events, Content, TextInput } from 'ionic-angular';

import firebase from 'firebase';

@IonicPage()
@Component({
    selector: 'page-userProfile',
    templateUrl: 'userProfile.html',
})
export class UserProfilePage {

    userId: string;
    userProfile: any;

    map: any = {};

    public loading:Loading;



    _isOpenEmojiPicker = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public ref: ChangeDetectorRef,
        public userProfileService: ContactService,
        public loadingCtrl: LoadingController,
    ) {
        this.loading = this.loadingCtrl.create();
        this.loading.present();

        this.userId = this.navParams.data.userId;
        if(!this.userId)
            this.userId = firebase.auth().currentUser.uid;
    }

    ionViewDidLoad() {
        this.map = {
            lat: 30.2672,
            lng: -97.7431,
            zoom: 12,
            markerLabel: "Austin"
        }
        this.loading.dismiss();
    }

    ionViewWillLeave() {
    }



    //this opens a new page where it will be a new form to change the things.
    openEditPage(){


        this.navCtrl.push(UserProfileEditPage,{ 
            userId: this.userId,
        });
    }



    ionViewDidEnter() {
        // this.userProfile = this.userProfileService.getUserProfile(this.userId);
        this.userProfileService.getUserProfile(this.userId).then(data => this.userProfile = data);
    }

    isNotOther(){
        return this.userId == firebase.auth().currentUser.uid;
    }

    getAddress() {
        let str = (this.userProfile.street||'') + ", " + (this.userProfile.city||'') + ", " + (this.userProfile.state||'') + " " + (this.userProfile.zip||'');
        if (str.length <= 5) 
            return "Unknown";
        return str;
    }
    ionViewLoaded(){

    }

    getDirections() {
        
    }
}
