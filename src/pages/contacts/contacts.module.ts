import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactsPage } from './contacts';
import { ContactService } from "../../providers/contact-service/contact-service";

@NgModule({
    declarations: [
        ContactsPage,
    ],
    imports: [
        IonicPageModule.forChild(ContactsPage),
    ],
    exports: [
        ContactsPage
    ],
    providers: [
        ContactService
    ]
})
export class ContactsPageModule { }
