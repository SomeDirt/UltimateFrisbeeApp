import { Component } from '@angular/core';

import { NavController, IonicPage } from 'ionic-angular';

import { ContactService, User } from "./../../providers/contact-service/contact-service";

import { UserProfilePage } from "./../userProfile/userProfile";




@IonicPage()
@Component({
    selector: 'page-contacts',
    templateUrl: 'contacts.html'
})
export class ContactsPage {

    public userList: Array<User>;
    public loadedUserList: Array<User>;

    icons: string[];

    constructor(public navCtrl: NavController, public contactService: ContactService) {

        //loads the userList
        this.userList = contactService.getUserList();
        //duplicates it to store it locally
        this.loadedUserList = this.userList;





    }//end constructor


    //this will reinitialize the list when searching for things
    initializeUsers() {
        this.userList = this.loadedUserList;
    }

    getUsers(searchbar) {
        //reset all items
        this.initializeUsers();


        //tmp is equal to the searchbar
        var tmp = searchbar.target.value;


        //if empty don't filter the items
        if (!tmp) {
            return;
        }

        this.userList = this.userList.filter((f) => {
            if (f.userName && tmp) {

                //search by name
                if (f.userName.toLowerCase().indexOf(tmp.toLowerCase()) > -1) {
                    return true;
                }
                //search by email
                else if (f.email.toLowerCase().indexOf(tmp.toLowerCase()) > -1) {
                    return true;

                }
                return false;
            }

        });

    }

    //this will open the profile of that user.
    //to be added.
    userTapped(event, user) {
        this.navCtrl.push(UserProfilePage, user);
    }




}
