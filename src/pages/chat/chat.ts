import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Events, Content, TextInput } from 'ionic-angular';
import { ChatService, ChatMessage } from "./../../providers/chat-service/chat-service";

import firebase from 'firebase';

@IonicPage()
@Component({
	selector: 'page-chat',
	templateUrl: 'chat.html',
})
export class Chat {

	//this is the content pane.
	@ViewChild(Content) content: Content;
	//this is the chat input. Where you type your messages
	@ViewChild('chat_input') messageInput: TextInput;
	//this is the array of chat messages, initialized to be null
	msgList: ChatMessage[] = [];
	//this is the current users UID
	userId: string;
	//this is the current users username.
	userName: string;
	//this is the current users img url
	userImgUrl: string;
	//this grabs a snapshot of the textbox text
	editorMsg: string = '';
	//this is for showing last seen msg.
	lastSeen = 0;
	//this is the name of the chat.
	chatName: string;
	//this is the uid of the chat, randomly generated.
	chatId: string;
	//this checks if the emoji picker is open.
	_isOpenEmojiPicker = false;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public chatService: ChatService,
		public events: Events,
		public ref: ChangeDetectorRef,

	) {


		// Get the navParams toUserId parameter
		this.chatId = navParams.get('chatId');
		this.chatName = navParams.get('chatName');



		// Get mock user information
		//set current user info.
		this.userId = firebase.auth().currentUser.uid;
		this.userName = firebase.auth().currentUser.displayName;
		this.userImgUrl = firebase.auth().currentUser.photoURL;
	}

	//it grabs the message.
	ionViewDidLoad() {
		// this.switchEmojiPicker();
		this.getMsg();

	}


	//the subscribe tells events which message was the last seen one.
	//it does that when the user leaves the view
	ionViewWillLeave() {
		// unsubscribe
		this.events.unsubscribe('chat:received')

	}

	//this method is called with the user enters this page view
	//it grabs the messages again and subscribes.
	ionViewDidEnter() {
		//get message list
		this.getMsg();
		this.events.subscribe('chat:received');
	}

	//i have no idea what this does. Found in a tutorial that didn't explain much.
	_focus() {
		this._isOpenEmojiPicker = false;
		this.content.resize();
	}

	//this changes the keyboard and toggles the emoji picker.
	switchEmojiPicker() {
		this._isOpenEmojiPicker = !this._isOpenEmojiPicker;
		if (!this._isOpenEmojiPicker) {
			this.messageInput.setFocus();
		}
		this.content.resize();
	}

	/**
	 * This gets the mesages from the chat service
	 * 
	* @name getMsg
	* @returns {Promise<ChatMessage[]>}
	*/
	getMsg() {

		return this.chatService
			.getMsgList(this.lastSeen, this.chatId)
			.then(res => {
				this.msgList = res;
			})
			.catch(err => {
				console.log(err)
			})
	}

	/**
	 * 
	 * this sends the messages to the chat service
	* @name sendMsg
	*/
	sendMsg() {
		if (!this.editorMsg.trim()) return;


		const id = Date.now().toString();

		let newMsg: ChatMessage = {
			messageId: Date.now().toString(),
			userId: this.userId,
			userName: this.userName,
			userImgUrl: this.userImgUrl,
			time: Date.now(),
			message: this.editorMsg,
			status: 'pending'
		};

		this.editorMsg = '';

		if (!this._isOpenEmojiPicker) {
			this.messageInput.setFocus();
		}

		this.chatService.sendMsg(newMsg, this.chatId)
			.then(() => {
				let index = this.getMsgIndexById(id);
				if (index !== -1) {
					this.msgList[index].status = 'success';
				}
			})

	}

	//this does things.
	getMsgIndexById(id: string) {
		return this.msgList.findIndex(e => e.messageId === id)
	}

	//this scrolls to the bottom.
	scrollDown() {
		this.content.scrollToBottom(0);
	}





}
