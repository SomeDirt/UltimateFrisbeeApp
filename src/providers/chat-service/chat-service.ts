import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Events } from 'ionic-angular';
import * as firebase from 'firebase';

import 'rxjs/add/operator/toPromise';

export class ChatMessage {
    messageId:string;
    userId: string;
    userName: string;
    userImgUrl: string;
    time: number|string;
    message: string;
    status: string;
}

@Injectable()
export class ChatService {
    ref;

    constructor(public http: Http, 
    public events: Events) {}

    getMsgList(lastSeen, chatId): Promise<ChatMessage[]> {
        var out = new Array();

         //sets firebase to messages subfolder
        this.ref = firebase.database().ref('chats/' + chatId + '/messages');
  
   //  reading data from firebase
  	    this.ref.on('value', data => {
  		    data.forEach( d => {          		
                if((d.val().time > lastSeen)){
                    let c : ChatMessage = {
                        messageId: d.val().messageId,
                        userId: d.val().userId,
                        userName: d.val().userName,
                        userImgUrl: d.val().userImgUrl,
                        time: d.val().time,
                        message: d.val().message,
                        status: d.val().success   
                    }

                    lastSeen = c.time;
                    out.push( c );
                }
  		    });

  	    });
        
      
        var p = new Promise<ChatMessage[]>((resolve, reject) => {
            resolve(out);
        })

        return p;
    }


    sendMsg(msg:ChatMessage, chatId:string){

         //sets firebase to messages subfolder
        this.ref = firebase.database().ref('chats/' + chatId + '/messages');


        return new Promise( (resolve,reject) => {
            setTimeout( () =>{
                resolve(msg)
            },Math.random()*1000)
        }).then(() => {
            this.ref.push(msg)
        })
    }




}
