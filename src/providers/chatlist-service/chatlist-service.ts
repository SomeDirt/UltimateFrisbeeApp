import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import * as firebase from 'firebase';

/*
  Generated class for the ChatlistServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

export class OpenChat {
    chatName: string;
    chatId: string;
    userIdList: string[];
    userList: string[];
}



@Injectable()
export class ChatlistService {
    ref;

    constructor(public http: Http) {

    }


    //checks to see if the user id is in any of the chats
    //returns an array of open chats.
    getChatList(userId) {
        var chatsLog = new Array<OpenChat>();

        this.ref = firebase.database().ref('chats');

        this.ref.once("value", function (snapshot) {
            snapshot.forEach(function (chats) {
                //create chat
                var chat: OpenChat = {
                    chatName: chats.val().chatName,
                    chatId: chats.val().chatId,
                    userIdList: chats.val().userIdList,
                    userList: chats.val().userIdList,
                }

                if (chat.chatName != null && chat.userIdList.indexOf(userId) != -1) {
                    chatsLog.push(chat);
                }


            })
        });
        return chatsLog;

    }


    //this cretes a new chat, then returns the id.
    createNewChat(chatName, userList, userIdList) {
        var chatId = this.generateRandomId();

        let newChat: OpenChat = {
            chatName,
            chatId,
            userIdList,
            userList
        }

        this.ref = firebase.database().ref('chats/' + chatId);

        this.ref.set(newChat);

        return chatId;


    }

    //generates a 16 char chat id
    generateRandomId() {

        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 16; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;



    }



}
