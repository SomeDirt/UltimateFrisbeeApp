import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as firebase from "firebase";

/*
  Generated class for the ContactServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

export class User {
    userId: string;
    userName: string;
    userImgUrl: string;
    phoneNumber: string;
    email: string;
}


@Injectable()
export class ContactService {
    userRef;

    constructor(public http: Http) {
        this.userRef = firebase.database().ref('userProfile');
    }



    getUserList() {
        this.userRef = firebase.database().ref('userProfile');

        //loading the values from firebase
        let userArray = [];
        this.userRef.once('value', users => {
            users.forEach(user => {
                let temp: User = {

                    userId: user.val().id,
                    userName: user.val().name,
                    userImgUrl: user.val().imgUrl,
                    phoneNumber: user.val().phoneNumber,
                    email: user.val().email,

                }

                if (temp.userId != firebase.auth().currentUser.uid) {
                    userArray.push(temp);
                }


            });

        });

        return userArray;

    }

    getUserProfile(id: string): any {
        this.userRef = firebase.database().ref('userProfile/' + id);
        return new Promise(resolve => {
            this.userRef.on('value', user => {
                resolve(user.val());

            });
        });
    }

    changePassword(id: string, oldPassword: string, newPassword: string) {
        this.getUserProfile(id).then(userProfile => {
            let encryptedPwd = this.encrypt(oldPassword, userProfile.passwordKey);
            if (encryptedPwd == userProfile.encrypetedPassword) {
                //update pwd in firebase

            }
        });
    }

    saveAddress(userProfile: any) {
        let userRef = firebase.database().ref('userProfile/' + userProfile.id);
        userRef.update({ street: userProfile.street, city: userProfile.city, state: userProfile.state, zip: userProfile.zip });

    }

    encrypt(text: string, key: string): boolean {
        return true;
    }


}
