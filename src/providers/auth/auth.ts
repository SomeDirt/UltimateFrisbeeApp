import { Injectable } from '@angular/core';
import firebase from 'firebase';

@Injectable()
export class AuthProvider {

  constructor() {}

  /**
   * loginUser takes in an email and password and signs the user into the application.
   */
  loginUser(email: string, password: string): firebase.Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  /**
   * signupUser takes in an email and password and does 3 things:
   * 1. It creates the new user.
   * 2. It signs the user into the application.
   * 3. It creates a database node for the user to store the userProfile, which starts with just
   *    the email address.
   */
  signupUser(email: string, password: string, phoneNumber: string, name: string): firebase.Promise<any> {

    return firebase.auth().createUserWithEmailAndPassword(email, password).then((newUser) => {
      firebase.database().ref('/userProfile/' + newUser.uid).set({
          id: newUser.uid,
          name: name,
          email: email,
          phoneNumber: "(" + phoneNumber.substring(0, 3) + ") " + phoneNumber.substring(3, 6) + " - " + phoneNumber.substring(6, 10),
          imgUrl: "https://firebasestorage.googleapis.com/v0/b/ultimate-firsbee-planner.appspot.com/o/default-user-icon-profile.png?alt=media&token=623fa1a1-3ffc-4acb-a068-74f6b0783eda"
      });
    });

 
  }

  /**
   * resetPassword takes the email address as a string and sends the email with the reset password 
   * link.
   */
  resetPassword(email: string): firebase.Promise<any> {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  /**
   * logoutUser doesn't take any parameters, it looks into the authentication object and signs out
   * the currently logged in user.
   */
  logoutUser(): firebase.Promise<any> {
    return firebase.auth().signOut();
  }

}
