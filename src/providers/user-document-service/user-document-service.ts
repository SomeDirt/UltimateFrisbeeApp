import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import { Camera } from '@ionic-native/camera';

/*
  Generated class for the UserImageServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserDocumentService {

    storageRef;
    databaseRef;

    constructor(public http: Http,
        private camera: Camera, ) {
        this.storageRef = firebase.app().storage().ref('/');
        this.databaseRef = firebase.database().ref('userProfile');
    }


    getUserImage(addr: string) {
        return "get image";
    }


    uploadNewProfileImage(imageString) {
        return new Promise((resolve, reject) => {
            let image: string = 'profile-' + new Date().getTime() + '.jpg';
            var parseUpload: any;
            var userId = firebase.auth().currentUser.uid;


            this.storageRef = firebase.storage().ref(userId + '/profile/' + image);

            parseUpload = this.storageRef.putString(imageString, 'data_url');

            parseUpload.on('state_changed', (snapshot) => {
                // We could log the progress here IF necessary
                console.log('snapshot progess ' + snapshot);
            },
                (error) => {
                    console.error("ERROR UPLOAD ->" + JSON.stringify(error));
                    reject(error);
                }, () => {

                    this.storageRef.getDownloadURL().then(function (URL) {
                        firebase.database().ref('/userProfile/' + userId).update({
                            // id: userId,
                            // name: "TEST",
                            // email: "TEST",
                            // phoneNumber: "TEST",
                            imgUrl:URL
                        });

                    });



                    resolve(parseUpload.snapshot);
                });
        });//end promise
    }


    profilePhoto: string;

    selectPhoto() {
        return new Promise(resolve => {
            this.camera.getPicture({
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                destinationType: this.camera.DestinationType.DATA_URL,
                quality: 100,
                targetHeight: 450,
                targetWidth: 450,
                encodingType: this.camera.EncodingType.JPEG,
                correctOrientation: true


            }).then(imageData => {
                this.profilePhoto = "data:image/jpeg;base64," + imageData;
                //Pass photo to firebase here.

                resolve(this.profilePhoto);

            }, error => {
                console.log("ERROR SELECT -> " + JSON.stringify(error));
            });
        })
    }

    takePhoto() {
        return new Promise(resolve => {
            this.camera.getPicture({
                quality: 100,
                destinationType: this.camera.DestinationType.DATA_URL,
                sourceType: this.camera.PictureSourceType.CAMERA,
                encodingType: this.camera.EncodingType.JPEG,
                //editing
                allowEdit: true,
                targetHeight: 450,
                targetWidth: 450,

                //for now this is testing.
                saveToPhotoAlbum: true

            }).then(imageData => {
                this.profilePhoto = "data:image/jpeg;base64," + imageData;
                //pass photo to firebase here

                resolve(this.profilePhoto);



            }, error => {
                console.log("ERROR -> " + JSON.stringify(error));
            });

        });
    }




}
