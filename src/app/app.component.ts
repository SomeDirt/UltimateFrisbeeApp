import { Component, NgZone, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';

import { HomePage } from "../pages/home/home";
import { OpenChatsPage } from "../pages/open-chats/open-chats";
import { ContactsPage } from "../pages/contacts/contacts";
import { UserProfilePage } from "../pages/userProfile/userProfile";


@Component({
  templateUrl: 'app.html'
})
export class UltimateFrisbeePlanner {
@ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;
  public zone:NgZone;


  pages: Array<{title: string, component: any}>;


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    this.zone = new NgZone({});
    const config = {
      apiKey: "AIzaSyCcpcmMPgZpYxt2eaW6DxVkBIqPSn9zte8",
      authDomain: "ultimate-firsbee-planner.firebaseapp.com",
      databaseURL: "https://ultimate-firsbee-planner.firebaseio.com",
      projectId: "ultimate-firsbee-planner",
      storageBucket: "ultimate-firsbee-planner.appspot.com",
      messagingSenderId: "887440241003"
    };
    
    firebase.initializeApp(config);

    firebase.auth().onAuthStateChanged( user => {
      this.zone.run( () => {
        if (!user) { 
          this.rootPage = 'LoginPage';
        } else {
          this.rootPage = HomePage;
        }
      });     
    });



    this.pages = [
      {title: 'My Profile', component: UserProfilePage},
      {title: 'Home', component: HomePage},
      {title: 'Contacts', component: ContactsPage},
      {title: 'Active Chats', component: OpenChatsPage},
    ];




    platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

  }


    openPage(page){
      this.nav.setRoot(page.component);
    }

}

