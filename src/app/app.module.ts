import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { HomePage } from '../pages/home/home';

import { LoginPageModule } from "../pages/login/login.module"; 
import { ChatModule } from "../pages/chat/chat.module";
import { ChatCreatePageModule } from "../pages/chat-create/chat-create.module";
import { ContactsPageModule } from "../pages/contacts/contacts.module";
import { OpenChatsPageModule } from "../pages/open-chats/open-chats.module";
import { UserProfileModule } from "../pages/userProfile/userProfile.module";
import { UserProfileEditModule } from "../pages/user-profile-edit/user-profile-edit.module";

import { UltimateFrisbeePlanner } from './app.component';

import { AuthProvider } from '../providers/auth/auth';
import { HttpModule } from "@angular/http";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Preloader } from '../providers/preloader/preloader';
import { UserDocumentService } from "../providers/user-document-service/user-document-service";


@NgModule({
  declarations: [
    UltimateFrisbeePlanner,
    HomePage,

  ],
  imports: [
    BrowserModule,
    HttpModule,

    ContactsPageModule,
    OpenChatsPageModule,
    ChatModule,
    ChatCreatePageModule,
    UserProfileModule,
    UserProfileEditModule,
    LoginPageModule,


    IonicModule.forRoot(UltimateFrisbeePlanner),


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    UltimateFrisbeePlanner,
    HomePage,

    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    Preloader,
    UserDocumentService,

  ]
})
export class AppModule {}
